export async function sum(...numbers:number[]) : Promise<number>{
    return numbers.reduce((total,curr)=> total + curr);
}

export async function multiply(m:number , ...numbers:number[]) : Promise<number[]>{
    return numbers.map(x => x*m);
}