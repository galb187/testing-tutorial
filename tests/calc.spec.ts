import { expect } from "chai";
import { sum, multiply} from "../src/calc";


const delay = (ms: number) =>
    new Promise((resolve) => setTimeout(resolve, ms));


describe("Test calc module",()=>{
    context("#sum", ()=>{
        it("should exist",async ()=>{
            expect(sum).to.be.instanceOf(Function);
        });

        it("should sum two numbers",async ()=>{
            await delay(1500);
            const actual = await sum(7,8);
            expect(actual).to.equal(15);
        });

        it("should sum several numbers", async ()=>{
            await delay(1500);
            const actual = await sum(7,7,7,7,7);
            expect(actual).to.equal(35);
        });
    });

    context("#multiply", ()=>{
        it("should exist", async()=>{
            expect(multiply).to.be.instanceOf(Function);
        });

        it("should multiply two numbers", async ()=>{
            await delay(1500);
            const actual = await multiply(7,7);
            expect(actual).to.eql([ 49 ]);
        });

        it("should multiply several numbers by number",async ()=>{
            await delay(1500);
            const actual = await multiply(2,1,2,3);
            expect(actual).to.eql([2,4,6]);
        });
    });
});

